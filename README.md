# Interest Calculator 

A very rudimentary web app, with minimal dependencies. Powered by VanillaJS.

## Dependencies

[Picocss](https://picocss.com/), a minimal CSS Framework 

## Installation

No installation needed, node_modules is commited (since it's ~ 3MB total)

## Usage

Simply clone the git repository, you can open the index.html directly in your browser.
Alternatively, you can use the http server of your choice.

## Open source licensing info
1. [LICENSE](LICENSE)
