"use strict";

let incomeEl = document.getElementById('income');
let contributionEl = document.getElementById('contribution');
let displayEls = document.getElementsByClassName('display')

let calculateTimeout;
let monthlyInterestRate = 1 + (3 / 100 / 12);
function calculate(){
  
  clearTimeout(calculateTimeout);
  let income = incomeEl.value;
  let contribution = contributionEl.value;

  if(!income){
    for(let i=0; i<displayEls.length;i++){
      displayEls[i].innerHTML = "";
      displayEls[i].setAttribute('aria-busy', false);
    }
    return;
  }
  
  for(let i=0; i<displayEls.length;i++){
      displayEls[i].setAttribute('aria-busy', true);
  }

  calculateTimeout = setTimeout(() => {
    let monthlyRate = income * contribution * 2 / 100;
    let accumulation = 0;
    console.log(monthlyRate);
    for(let i=0; i<displayEls.length;i++){
      for(let j=1; j<=12; j++){
        accumulation += monthlyRate; 
        accumulation *= monthlyInterestRate;
      }
      displayEls[i].innerHTML = (Math.round(accumulation * 100) / 100) + '€';
      displayEls[i].setAttribute('aria-busy', false);
    }
  }, 1500)
} 
